#!/usr/bin/env python3
from hashlib import md5


class Episode:
    def __init__(self, title, page_url, image_url, broadcasting_date, duration="" ):
        self.__title = title
        self.__page_url = page_url
        self.__image_url = image_url
        self.__broadcasting_date = broadcasting_date
        self.duration = duration
        self.__episode_id = self.__parse_episode_id(image_url)
        self.__key = self.__generate_key(page_url)

    '''
    Properties of class
    '''
    @property
    def title(self):
        return self.__title
    @property
    def page_url(self):
        return "http://www.tv5monde.com" + self.__page_url
    @property
    def image_url(self):
        return self.__image_url
    @property
    def broadcasting_date(self):
        return self.__broadcasting_date
    @property
    def episode_id(self):
        return self.__episode_id
    @property
    def key(self):
        return self.__episode_id if self.__episode_id else self.__key

    @staticmethod
    def from_json(payload):
        return Episode(payload["title"], payload["url"], payload["image"], payload["date"], payload["duration"])

    @staticmethod
    def __parse_episode_id(url):
        if url and isinstance(url, str):
            img_name = url.split('/')[-1]
            return '.'.join(img_name.split('.')[:-1])
        return ''

    @staticmethod
    def __generate_key(s):
        if isinstance(s, str):
            return md5(s.encode()).hexdigest()
