#!/usr/bin/env python3
from Episode import Episode
import json

payload = json.loads("""{
          "title" : "Faire une course et faire ses courses",
          "image" : "",
          "url" : "/emissions/episode/merci-professeur-faire-une-course-et-faire-ses-courses",
          "duration" : "02:10",
          "date" : "Jeudi 12 septembre 2019"
       }""")

# print(tuple(payload.values()))
episode = Episode.from_json(payload)
print(episode.episode_id)
print(episode.key)
# print(episode.title)
# print(episode.page_url)
# print(episode.image_url)
# print(episode.broadcasting_date)
# print(episode.duration)
# print(episode.episode_id)

# episode.title = "STE"
# print(episode.title)
# episodes = fetch_episodes(
#     "http://www.tv5monde.com/emissions/episodes/merci-professeur.json")
# episode = episodes[2]

# payload = loads("""{
#           "title" : "Faire une course et faire ses courses",
#           "image" : "https://vodhdimg.tv5monde.com/tv5mondeplus/images/5148922.jpg",
#           "url" : "/emissions/episode/merci-professeur-faire-une-course-et-faire-ses-courses",
#           "duration" : "02:10",
#           "date" : "Jeudi 12 septembre 2019"
#        }""")
# episode = Episode.from_json(payload)
# html_content = fetch_episodes_html_page(episode)
# data = parse_broadcast_data_attribute(html_content)
# print(data)
# build_segment_url_pattern(data)
# print(episode.page_url)
# html_page = fetch_episodes_html_page(episode)
# print(parse_broadcast_data_attribute(html_page))

# html_context = read_url('http://www.tv5monde.com/emissions/episode/merci-professeur-trace')

# print(html_context)
# print(type(html_context))
