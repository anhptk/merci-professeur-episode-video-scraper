#!/usr/bin/env python3
from Episode import Episode
from requests import get
from requests.exceptions import HTTPError
from time import sleep
from json import loads, dumps
from os.path import abspath, getsize, exists
from os import open, O_CREAT, O_WRONLY, O_TRUNC, O_APPEND, O_RDONLY, O_RDWR, write, close, read
from caching import segments_in_cache, video_in_cache, update_cache, load_cache


def get_request(
        url,
        maximum_attempt_count=3,
        sleep_duration_between_attempts=10):
    try:
        for i in range(maximum_attempt_count):
            # put in real browser headers
            header = {
                "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36"}
            req = get(url, headers=header)

            if not 499 < req.status_code < 600:  # what are temporary issues?
                break
            print("Temporary issue occured. Waiting for reconnect...")
            sleep(sleep_duration_between_attempts)

        # raise Exception if req not valid
        req.raise_for_status()
        return req
    except HTTPError as error:
        print(error)
    except Exception as error:
        print(error)


def read_url(
        url,
        maximum_attempt_count=3,
        sleep_duration_between_attempts=10):
    """
    Return data fetched from a HTTP endpoint.


    :param url: A Uniform Resource Locator (URL) that references the
        endpoint to open and read data from.

    :param maximum_attempt_count: Maximal number of failed attempts to
        fetch data from the specified URL before the function raises an
        exception.

    :param sleep_duration_between_attempts: Time in seconds during which
        the current thread is suspended after a failed attempt to fetch
         data from the specified URL, before a next attempt.


    :return: The data read from the specified URL.


    :raise HTTPError: If an error occurs when trying unsuccessfully
        several times to fetch data from the specified URL, after
    """

    req = get_request(url, maximum_attempt_count,
                      sleep_duration_between_attempts)

    try:
        episodes_data = req.json()
        return episodes_data
    except ValueError:
        return {}


def fetch_episodes(url):
    episodes_data = read_url(url)
    if "numPages" in episodes_data and "episodes" in episodes_data:
        numpages = episodes_data['numPages']
        episode_list = []
        for i in range(1, numpages+1):
            for ep in episodes_data['episodes']:
                episode_list.append(Episode.from_json(ep))
            new_url = url + "?page=" + str(i + 1)
            episodes_data = read_url(new_url)
        return episode_list
    return None


def fetch_episodes_html_page(episode):
    try:
        if not isinstance(episode, Episode):
            raise TypeError("Can only fetch Episode object.")
        req = get_request(episode.page_url)
        req.encoding = "UTF-8"
        return req.text
    except Exception as error:
        print(error)


def parse_broadcast_data_attribute(html_page):
    try:
        if not isinstance(html_page, str):
            raise TypeError("HTML data must be a string.")
    # get string json data
        raw_data = html_page.split('data-broadcast=')[1]
        data_broadcast = raw_data.split(raw_data[0])[1]
        return loads(data_broadcast)
    except Exception:
        return None


def build_segment_url_pattern(data_broadcast):
    try:

        if not isinstance(data_broadcast, dict):
            raise TypeError("Data broadcast must be a dictionary.")
        url = data_broadcast['files'][0]['url']
        if data_broadcast["files"][0]["format"] == "m3u8":
            url_frags = url.split('/')
            url_frags[-1] = 'segment{}_3_av.ts?null=0'
            url = '/'.join(url_frags)
        return url
    except Exception:
        return None



def download_episode_video_segments(episode, path='.'):
    def download_single_segment(request, segment_path):
        req_bytes = request.content
        fd = open(segment_path, O_CREAT | O_WRONLY)
        write(fd, req_bytes)
        close(fd)
    try:

        # check data tyoes
        if not (isinstance(episode, Episode) and isinstance(path, str)):
                raise TypeError("Wrong parameter data types.")

        # check for caches
        caches = load_cache()
        if segments_in_cache(episode.key, caches):
            return caches[episode.key]["segments"]

        # get segment urls 
        html_content = fetch_episodes_html_page(episode)
        data = parse_broadcast_data_attribute(html_content)
        segment_url = build_segment_url_pattern(data)

        # get segment path name
        dir_name = abspath(path) if path.endswith('/') else abspath(path) + '/'
        segments = []
        # download segments playlist
        if data["files"][0]["format"] == "m3u8":
            # segment file name
            segment_path = dir_name + "segment_" + episode.key +  "_{}.ts"

            # init data
            i = 1
            while True:
                req = get_request(segment_url.format(str(i)))
                if req:
                    download_single_segment(req, segment_path.format(str(i)))
                    segments.append(segment_path.format(str(i)))
                    i += 1
                else:
                    update_cache(episode.key, segments)
                    break

        # download video
        elif data["files"][0]["format"] == "mp4":
            segment_path = dir_name + episode.key + '.mp4'
            req = get_request(segment_url)
            if req:
                download_single_segment(req, segment_path)
                segments.append(segment_path)
                update_cache(episode.key, segments, segment_path)
        return segments
    except Exception as error:
        print(error)


def build_episode_video(episode, segment_file_path_names, path=''):
    try:
        if not (isinstance(episode, Episode) and isinstance(segment_file_path_names, list) and isinstance(path, str)):
            raise TypeError("Wrong parameter data types.")

        # check for caches
        caches = load_cache()
        if video_in_cache(episode.key, caches):
            return caches[episode.key]["video"]

        # get the final video path
        if path:
            dir_name = abspath(path) if path.endswith('/') else abspath(path) + '/'
        else:
            segment_path_frags = segment_file_path_names[0].split('/')
            segment_path_frags[-1] = ''
            dir_name = '/'.join(segment_path_frags)
        filename = dir_name + episode.key + '.ts'

        # create new file with new content
        fd = open(filename, O_CREAT | O_TRUNC)
        close(fd)

        # move data from segments
        main = open(filename, O_APPEND | O_RDWR)
        for pathname in segment_file_path_names:
            fd = open(pathname, O_RDONLY)
            content = read(fd, getsize(pathname))
            write(main, content)
            close(fd)
        close(main)

        #update cache
        update_cache(episode.key, [], filename)
        return filename
    except Exception as error:
        print(error)


episodes = fetch_episodes(
    "http://www.tv5monde.com/emissions/episodes/merci-professeur.json")

episode = episodes[0]
print(episode.key)
segments = download_episode_video_segments(episode)
build_episode_video(episode, segments)

