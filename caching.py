#!/usr/bin/env python3
from os.path import exists
from json import loads, dumps


def load_cache():
    cache_path='./.video_cache.txt'
    if exists(cache_path):
        fd = open(cache_path, 'r')
        cache = fd.read()
        fd.close()
        caches = loads(cache)
    else:
        caches = {}
    return caches


def write_cache(caches):
    if caches:
        cache_path='./.video_cache.txt'
        fd = open(cache_path, 'w+')
        fd.write(dumps(caches, indent=4))
        fd.close()


def create_new_cache(episode_id, segments, video_path, caches):
    caches[episode_id] = {"segments": segments, "video": video_path}
    return caches


def update_cache(episode_id, segments=[], video_path=''):
    caches = load_cache()
    if episode_id not in caches:
        caches = create_new_cache(episode_id, segments, video_path, caches)
    else:
        if segments:
            caches[episode_id]["segments"] = segments
        if video_path:
            caches[episode_id]["video"] = video_path
    write_cache(caches)
    return caches


def segments_in_cache(episode_id, caches):
    if episode_id in caches and caches[episode_id]["segments"]:
        return True
    return False


def video_in_cache(episode_id, caches):
    if episode_id in caches and caches[episode_id]["video"]:
        return True
    return False

